﻿using UnityEngine;


public class PlayerMovement : MonoBehaviour { 

    //Fizička komponenta objekta
    Rigidbody crazyFlie;

    

    //Sila koja ide prema gore 
    public float upForce;
    //Konstanta sile prema gore, otpor gravitaciji odsnosno masa*gravitacija, kostanta
    private float upForceConst;
    //Gravitacija 
    private float gravity=9.81f;

    //Brzina kretnje prema naprijed
    public float msForward = 0.2f;
    //Vrijednost nagiba
    private float tiltForward = 0f;
    //Veličina kuta nagiba
    public float tiltAngleFoward = 20f;
    //Vrijednost brzine, varijabla napravljena samo za korištenje u SmoothDamp funkciji
    private float tiltVelocityForward;
    //Radian tiltFoward 
    private float tiltFowardRad;



    //Brzina kretnje u stranu 
    public float sideMovementAmount = 0.2f;
    //Vrijednost nagiba u stranu 
    private float tiltSideways = 0f;
    //Vrijednost kuta nagiba u stranu
    private float tiltAmoutSidewaysAngle = 20;
    //Vrijednost brzine, varijabla napravljena samo za korištenje u SmoothDamp funkciji
    private float tiltVelocitySideways;
    //Radian tiltFoward 
    private float tiltSidewaysRad;

    //Sila prilikom zakrivljenja unaprijed
    private float crazyFlieTiltAmounntFowardForce;
    //Sila prilikom zakrivljenja u stranu 
    private float crazyFlieSideAmountForce;



    //Željena Y rotacija koju želimo ostvariti
    private float wantedYRotation;
    //Trenutna Y rotacija
    public float currentYRotion;
    //Količina rotiranja konstanta za inkrement
    private float rotateAmout = 2.0f;
    //Brzina rotiranja(smoothdamp)
    private float rotationYVelocity;

    private Vector3 velocityToSmoothDampToZero;
    
 

    //Inicijalizacija osnovnih konstanti
    private void Start()
    {
        //Dohvaćamo fizičku komponentu objekta 
        crazyFlie = GetComponent<Rigidbody>();
        upForceConst = crazyFlie.mass*gravity;
        upForce = upForceConst;

    }
    /*Metoda koja se konstantno izvodi i     
     *  poziva se isti broj puta kod svakog računala, bez obzira na snagu računala  
     */
    private void FixedUpdate()
    {
        UpDownMovement();
        FowardMovement();
        Rotation();
        ClampingSpeed();
        SideMovement();


        //Količina sile koju dodajemo prema gore 60 puta u sekundi
        crazyFlie.AddRelativeForce(Vector3.up * upForce);

        //Rotira crazyflie objekt za kut koji se objekt rotira po x,y,z
        // Quaternion.Euler vraća vrijednost rotacija (Vector3) po x,y,z
        crazyFlie.rotation = Quaternion.Euler(new Vector3(tiltForward,currentYRotion,tiltSideways));

        


    }

    //Metoda za kontroliranje kretnji prema gore i dolje
    private void UpDownMovement() {

        if (Mathf.Abs(Input.GetAxis("Vertical")) > 0.1f || Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1f)
        {
            
           
            if ((Input.GetKey(KeyCode.I) || Input.GetKey(KeyCode.K))|| (Mathf.Abs(Input.GetAxis("LeftTrigger")) > 0.1f || Mathf.Abs(Input.GetAxis("RightTrigger")) > 0.1f))
            {
                crazyFlie.velocity = crazyFlie.velocity;
            }
            if ((!Input.GetKey(KeyCode.I) && !Input.GetKey(KeyCode.K) && !Input.GetKey(KeyCode.J) && !Input.GetKey(KeyCode.L))|| (Mathf.Abs(Input.GetAxis("LeftTrigger")) < 0.1f && Mathf.Abs(Input.GetAxis("RightTrigger")) < 0.1f && Mathf.Abs(Input.GetAxis("LeftGrip")) < 0.1f && Mathf.Abs(Input.GetAxis("RightGrip")) < 0.1f))
            {
                crazyFlie.velocity = new Vector3(crazyFlie.velocity.x, Mathf.Lerp(crazyFlie.velocity.y, 0, Time.deltaTime * 5), crazyFlie.velocity.z);
                upForce = upForce;
            }
            if ((!Input.GetKey(KeyCode.I) && !Input.GetKey(KeyCode.K) && !Input.GetKey(KeyCode.J) || !Input.GetKey(KeyCode.L))|| (Mathf.Abs(Input.GetAxis("LeftTrigger")) < 0.1f && Mathf.Abs(Input.GetAxis("RightTrigger")) < 0.1f && Mathf.Abs(Input.GetAxis("LeftGrip")) < 0.1f || Mathf.Abs(Input.GetAxis("RightGrip")) < 0.1f))
            {
                crazyFlie.velocity = new Vector3(crazyFlie.velocity.x, Mathf.Lerp(crazyFlie.velocity.y, 0, Time.deltaTime * 5), crazyFlie.velocity.z);
                upForce = upForce;
            }
            if ((!Input.GetKey(KeyCode.J) || !Input.GetKey(KeyCode.L))|| (Mathf.Abs(Input.GetAxis("LeftGrip")) < 0.1f || Mathf.Abs(Input.GetAxis("RightGrip")) < 0.1f))
            {

                upForce = upForce;
            }
        }
        else {
            upForce = upForceConst;
        }
        if (Mathf.Abs(Input.GetAxis("Vertical")) <0.1f && Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1f)
        {
            upForce = upForce;
        }
        else {
            upForce = upForceConst;
        }

        if ((Input.GetKey(KeyCode.I) || (Mathf.Abs(Input.GetAxis("LeftTrigger")) > 0.1f)) && Input.GetAxis("Horizontal") > 0.1f)
        {
            upForce = 0.4f;
        }
        else {

            upForce = upForceConst;
        }
     
        if (Input.GetKey(KeyCode.I) || (Mathf.Abs(Input.GetAxis("LeftTrigger")) > 0.1f))
        {
            upForce = 0.4f;
     
        }
        else if (Input.GetKey(KeyCode.K)|| (Mathf.Abs(Input.GetAxis("RightTrigger")) > 0.1f))
        {
            upForce = -0.4f;
        }
        else if(((!Input.GetKey(KeyCode.I) && !Input.GetKey(KeyCode.K) && Mathf.Abs(Input.GetAxis("Vertical"))<0.1f && Mathf.Abs(Input.GetAxis("Horizontal")) < 0.1f))|| (Mathf.Abs(Input.GetAxis("LeftTrigger")) < 0.1f && Mathf.Abs(Input.GetAxis("RightTrigger")) < 0.1f && Mathf.Abs(Input.GetAxis("Vertical")) < 0.1f && Mathf.Abs(Input.GetAxis("Horizontal")) < 0.1f)) {  
            upForce = upForceConst;    
        }

    }
   
    

    //MetodaZa kretanje naprijed
    public void FowardMovement() {
        if (Input.GetAxis("Vertical")!=0)
        {
            /*Dodajemo silu u smijeru z osi, određujemo preznak preko Input.getAxis("Vertical") koji vraća vrijednosti -1,0,1 ovisno idemo li gore 
            il prema dolje i msForward koji praktički određuje brzinu 
            */
            crazyFlie.AddRelativeForce(0, upForceConst, crazyFlieTiltAmounntFowardForce* Input.GetAxis("Vertical")*msForward);

            /*Mathf.SmoothDamp funkcija postepeno mijenja vrijednosti vektora prema željenoj u određeno vrijeme
             *Vector3 SmoothDamp(Vector3 trenutnaVrijednost, Vector3 željenaVrijednost, ref Vector3 trenutnaBrzina, float smoothTime, float maxSpeed = Mathf.Infinity, float deltaTime = Time.deltaTime);
             */
            tiltForward = Mathf.SmoothDamp(tiltForward, tiltAngleFoward* Input.GetAxis("Vertical"), ref tiltVelocityForward, 0.1f);
            tiltFowardRad = (tiltForward / 180f) * Mathf.PI;
            //Sila koja dijeluje na objekt prilikom tilta odnosno nagiba prema naprijed,
            crazyFlieTiltAmounntFowardForce = Mathf.Cos(tiltFowardRad) * upForceConst;
        }
        else
        {
            tiltForward = Mathf.SmoothDamp(tiltForward, 0, ref tiltVelocityForward, 0.1f);
        }
    }
    //Metoda za nagib 
    private void SideMovement()
    {
        if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1f)
        {
            crazyFlie.AddRelativeForce(crazyFlieSideAmountForce * Input.GetAxis("Horizontal") * sideMovementAmount, upForceConst, 0);
            tiltSideways = Mathf.SmoothDamp(tiltSideways, -1 * tiltAmoutSidewaysAngle * Input.GetAxis("Horizontal"), ref tiltVelocitySideways, 0.1f);

            tiltSidewaysRad = (tiltSideways / 180f) * Mathf.PI;
            crazyFlieSideAmountForce = Mathf.Cos(tiltSidewaysRad) * upForceConst;
        }
        else
        {
            tiltSideways = Mathf.SmoothDamp(tiltSideways, 0, ref tiltVelocitySideways, 0.1f);
        }

    }
    //Funkcija zadužena za rotaciju
    private void Rotation() {
        if (Input.GetKey(KeyCode.J)|| (Mathf.Abs(Input.GetAxis("LeftGrip")) > 0.1f)) {
            wantedYRotation -= rotateAmout;

        }
        if (Input.GetKey(KeyCode.L)|| (Mathf.Abs(Input.GetAxis("RightGrip")) > 0.1f))
        {
            wantedYRotation += rotateAmout;

        }
        currentYRotion = Mathf.SmoothDamp(currentYRotion, wantedYRotation, ref rotationYVelocity, 0.25f);

    }

    //Stežemo vrijednosti brzine drona
    //Ograničavamo vrijednosti brzine pomoću funkcije ClampMagnitude
    private void ClampingSpeed() {


        if (Mathf.Abs(Input.GetAxis("Vertical")) > 0.1f && Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1f) { 
            crazyFlie.velocity = Vector3.ClampMagnitude(crazyFlie.velocity, Mathf.Lerp(crazyFlie.velocity.magnitude, 10.0f, Time.deltaTime * 5f));
        }
        if (Mathf.Abs(Input.GetAxis("Vertical")) > 0.1f && Mathf.Abs(Input.GetAxis("Horizontal")) < 0.1f) {
            crazyFlie.velocity = Vector3.ClampMagnitude(crazyFlie.velocity, Mathf.Lerp(crazyFlie.velocity.magnitude, 10.0f, Time.deltaTime * 5f));
        }
        if (Mathf.Abs(Input.GetAxis("Vertical")) < 0.1f && Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1f)
        {
            crazyFlie.velocity = Vector3.ClampMagnitude(crazyFlie.velocity, Mathf.Lerp(crazyFlie.velocity.magnitude, 5.0f, Time.deltaTime * 5f));
        }
        //Nijedna tipka nije aktivna, stoji u mjestu
        if (Mathf.Abs(Input.GetAxis("Vertical")) < 0.1f && Mathf.Abs(Input.GetAxis("Horizontal")) < 0.1f)
        {
            crazyFlie.velocity = Vector3.SmoothDamp(crazyFlie.velocity,Vector3.zero,ref velocityToSmoothDampToZero,0.95f);
        }
    }

    
  

}

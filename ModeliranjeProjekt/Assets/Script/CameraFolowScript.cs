﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFolowScript : MonoBehaviour {

    private Transform crazyFlie;
    private Vector3 velocityCameraFollow;
    public Vector3 behindPosition = new Vector3(0, 2, -4);
    public float angle;

    private void Awake()
    {
        crazyFlie = GameObject.FindGameObjectWithTag("Player").transform;
    }
	void FixedUpdate () {
        transform.position = Vector3.SmoothDamp(transform.position, crazyFlie.transform.TransformPoint(behindPosition) + Vector3.up * Input.GetAxis("Vertical"), ref velocityCameraFollow, 0.1f);
        transform.rotation = Quaternion.Euler(new Vector3(angle, crazyFlie.GetComponent<PlayerMovement>().currentYRotion, 0));
	}
}

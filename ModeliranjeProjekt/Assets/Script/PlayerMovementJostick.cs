﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class PlayerMovementJostick : MonoBehaviour {


    //Fizička komponenta crazyflie objekta
    Rigidbody crazyFlie;
  

    private float upForce;
    public float movementFowardSpeed = 200.00f;
    private float tiltAmountForward = 0;
    private float tiltVelocityForward;

    private float wantedYRotation;
    public float currentYRotion;

    private float rotateAmoutByKeys = 2.5f;
    private float rotationYVelocity;
    
    public float sideMovementAmount = 300f;

    private float tiltAmountSideways;
    private float tiltAmountVelocity;

    private Vector3 velocityToSmoothDampToZero;

    void Awake () {
        crazyFlie = GetComponent<Rigidbody>();
    }
	void FixedUpdate () {

        MovementUpDown();
        MovementFoward();
        Rotation();
        ClampingSpeedValues();
        Swerve();
        crazyFlie.AddRelativeForce(Vector3.up * upForce);

        crazyFlie.rotation = Quaternion.Euler(
            new Vector3(tiltAmountForward, currentYRotion, tiltAmountSideways)
            );


    }
    private void MovementUpDown()
    {
        
        if (Mathf.Abs(Input.GetAxis("Vertical")) > 0.1f || Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1f)
        {

            if (Mathf.Abs(Input.GetAxis("LeftTrigger"))>0.1f || Mathf.Abs( Input.GetAxis("RightTrigger"))> 0.1f)
            {
                crazyFlie.velocity = crazyFlie.velocity;
            }
            if (Mathf.Abs(Input.GetAxis("LeftTrigger")) < 0.1f && Mathf.Abs(Input.GetAxis("RightTrigger")) < 0.1f && Mathf.Abs(Input.GetAxis("LeftGrip")) < 0.1f && Mathf.Abs(Input.GetAxis("RightGrip")) < 0.1f)
            {
                crazyFlie.velocity = new Vector3(crazyFlie.velocity.x, Mathf.Lerp(crazyFlie.velocity.y, 0, Time.deltaTime * 5), crazyFlie.velocity.z);
                upForce = 281;
            }
            if (Mathf.Abs(Input.GetAxis("LeftTrigger")) < 0.1f && Mathf.Abs(Input.GetAxis("RightTrigger")) < 0.1f && Mathf.Abs(Input.GetAxis("LeftGrip")) < 0.1f || Mathf.Abs(Input.GetAxis("RightGrip")) < 0.1f)
            {
                crazyFlie.velocity = new Vector3(crazyFlie.velocity.x, Mathf.Lerp(crazyFlie.velocity.y, 0, Time.deltaTime * 5), crazyFlie.velocity.z);
                upForce = 110;
            }
            if (Mathf.Abs(Input.GetAxis("LeftGrip")) < 0.1f || Mathf.Abs(Input.GetAxis("RightGrip")) < 0.1f)
            {

                upForce = 410;
            }
        }
        if (Mathf.Abs(Input.GetAxis("Vertical")) < 0.1f && Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1f)
        {
            upForce = 135;
        }

        if (Mathf.Abs(Input.GetAxis("LeftTrigger")) > 0.1f)
        {
            upForce = 450;
            if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0.2f)
            {
                upForce = 500;
            }
        }
        if (Mathf.Abs(Input.GetAxis("RightTrigger")) > 0.1f)
        {
            upForce = -200;
        }
        if (Mathf.Abs(Input.GetAxis("LeftTrigger")) < 0.1f && Mathf.Abs(Input.GetAxis("RightTrigger"))< 0.1f && Mathf.Abs(Input.GetAxis("Vertical")) < 0.1f && Mathf.Abs(Input.GetAxis("Horizontal")) < 0.1f)
        {
            upForce = 9.81f;
        }

    }
    public void MovementFoward()
    {
        if (Input.GetAxis("Vertical") != 0)
        {
            crazyFlie.AddRelativeForce(Vector3.forward * Input.GetAxis("Vertical") * movementFowardSpeed);
            tiltAmountForward = Mathf.SmoothDamp(tiltAmountForward, 20 * Input.GetAxis("Vertical"), ref tiltVelocityForward, 0.1f);
        }


    }


    private void Rotation()
    {
        if (Mathf.Abs(Input.GetAxis("LeftGrip")) > 0.1f)
        {
            wantedYRotation -= rotateAmoutByKeys;

        }
        if (Mathf.Abs(Input.GetAxis("RightGrip")) > 0.1f)
        {
            wantedYRotation += rotateAmoutByKeys;

        }
        currentYRotion = Mathf.SmoothDamp(currentYRotion, wantedYRotation, ref rotationYVelocity, 0.25f);

    }
    private void ClampingSpeedValues()
    {
        if (Mathf.Abs(Input.GetAxis("Vertical")) > 0.2f && Mathf.Abs(Input.GetAxis("Horizontal")) > 0.2f)
        {

            crazyFlie.velocity = Vector3.ClampMagnitude(crazyFlie.velocity, Mathf.Lerp(crazyFlie.velocity.magnitude, 10.0f, Time.deltaTime * 5f));
        }
        if (Mathf.Abs(Input.GetAxis("Vertical")) > 0.2f && Mathf.Abs(Input.GetAxis("Horizontal")) < 0.2f)
        {
            crazyFlie.velocity = Vector3.ClampMagnitude(crazyFlie.velocity, Mathf.Lerp(crazyFlie.velocity.magnitude, 10.0f, Time.deltaTime * 5f));
        }
        if (Mathf.Abs(Input.GetAxis("Vertical")) < 0.2f && Mathf.Abs(Input.GetAxis("Horizontal")) > 0.2f)
        {
            crazyFlie.velocity = Vector3.ClampMagnitude(crazyFlie.velocity, Mathf.Lerp(crazyFlie.velocity.magnitude, 5.0f, Time.deltaTime * 5f));
        }
        if (Mathf.Abs(Input.GetAxis("Vertical")) < 0.2f && Mathf.Abs(Input.GetAxis("Horizontal")) < 0.2f)
        {
            crazyFlie.velocity = Vector3.SmoothDamp(crazyFlie.velocity, Vector3.zero, ref velocityToSmoothDampToZero, 0.95f);
        }
    }
    private void Swerve()
    {
        if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0.2f)
        {

            crazyFlie.AddRelativeForce(Vector3.right * Input.GetAxis("Horizontal") * sideMovementAmount);
            tiltAmountSideways = Mathf.SmoothDamp(tiltAmountSideways, -1 * 20 * Input.GetAxis("Horizontal"), ref tiltAmountVelocity, 0.1f);
        }
        else
        {
            tiltAmountSideways = Mathf.SmoothDamp(tiltAmountSideways, 0, ref tiltAmountVelocity, 0.1f);
        }

    }
}
